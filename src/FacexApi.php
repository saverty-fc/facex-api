<?php

namespace AgileFC\FacexApi;

class FacexApi
{
    protected $APP_ID;
    protected $APP_KEY;
    protected $url = "http://facexapi.com/compare_faces?face_det=1";


    /**
     * Constructor
     *
     * @param String $appId Facex app id
     * @param String $appKey Facex app key
     */
    public function __construct(String $appId, String $appKey){
        $this->APP_ID = $appId;
        $this->APP_KEY = $appKey;
    }

    /**
     * Compare two image and return the confidence (Between 0 and 1)
     *
     * @param String $image1 Path of the first image
     * @param String $image2 Path of the second image
     * @return float
     */
    public function compare(String $image1, String $image2) : float{
        $imageObject1 = $this->makecUrlFile($image1);
        $imageObject2 = $this->makecUrlFile($image2);


        $request = curl_init();

        $imageObject =  array("img_1" => $imageObject1, "img_2" => $imageObject2);
        curl_setopt($request, CURLOPT_URL, $this->url);
        curl_setopt($request, CURLOPT_POST, true);
        curl_setopt($request, CURLOPT_HTTPHEADER, array(
            "content-type: multipart/form-data",
            "user_id:" . $this->APP_ID,
            "user_key:" . $this->APP_KEY
        )
        );
        curl_setopt($request,CURLOPT_POSTFIELDS,$imageObject);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($request);  // curl response
        curl_close($request);


        $response = json_decode($response);

        return (float) (isset($response->confidence)) ? $response->confidence : 0;
    }

    /**
     * Check two image and return true or false according with the confidence
     *
     * @param String $image1 Path of the first image
     * @param String $image2 Path of the second image
     * @param float $confidence
     * @return boolean
     */
    public function check(String $image1, String $image2, float $confidence = 0.7) : bool {
        if( json_decode($this->compare($image1, $image2))  >= $confidence){
            return true;
        }else{
           return false;
        }
    }

    protected function makecUrlFile($file){
        $mime = mime_content_type($file);
        $info = pathinfo($file);
        $name = $info['basename'];
        $output = new \CURLFile($file, $mime, $name);
        return $output;
    }
}
