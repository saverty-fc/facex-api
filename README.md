
#  Facex API 

## About

Facex api is a package that help you to use Facex, an api for the face recognition.

## Installation

    composer require agile-fc/facex-api

## Usage

### Calculate the confidence 

    <?php
    
    require  __DIR__  .  '/vendor/autoload.php';    
    
    use AgileFC\FacexApi\FacexApi;
    
    $faceX = new FacexApi("YOUR_APP_ID", "YOUR_APP_KEY");
    
    echo $faceX->compare(__DIR__.'/your_first_image.png', __DIR__.'/your_second_image.png');
	
	// return 0.75664

###   Check if the faces are the same.

     <?php
    
    require  __DIR__  .  '/vendor/autoload.php';    
    
    use AgileFC\FacexApi\FacexApi;
    
    $faceX = new FacexApi("YOUR_APP_ID", "YOUR_APP_KEY");
    
    echo $faceX->check(__DIR__.'/your_first_image.png', __DIR__.'/your_second_image.png', 0.700);
	
	// return true or false
	
  
 - [Linkedin](https://www.linkedin.com/in/steve-averty-64808a62/)
 - [Twitter](https://twitter.com/steveaverty)
